package main

import (
	"bitbucket.org/tryge/filer/domain"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

func helpRunFiler() {
	fmt.Printf("Synopsis: %s run [<options>]", os.Args[0]+" [--config=<db>]")
	fmt.Println()
	fmt.Println()
	fmt.Println("\truns the filer in foreground")
	fmt.Println()
	fmt.Println("Options:")
	fmt.Println("\t--http-port <int>           ... the port the http server is listening on, default is 8080")
	fmt.Println("\t--pid-file <file-path> ... the file where the pid is written to, default is 'filer.pid'")
	fmt.Println()
}

func runFiler(filer domain.Filer, args []string) {
	port, pidFile := readRunFlags(args)

	err := createPidFile(pidFile)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(3)
	}
	defer removePidFile(pidFile)

	go configurationReloader(filer)

	http.Handle("/", http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		parts := strings.SplitN(strings.TrimLeft(req.URL.Path, "/"), "/", 2)
		if len(parts) != 2 {
			res.WriteHeader(http.StatusNotFound)
			return
		}

		name := parts[0]
		file := parts[1]

		box, err := filer.GetBox(name)
		if err != nil {
			res.WriteHeader(http.StatusNotFound)
			return
		}

		if req.Method == "GET" {
			archive, err := box.Get(file)
			if err != nil {
				res.WriteHeader(http.StatusNotFound)
				return
			}

			reader, err := archive.Reader()
			if err != nil {
				res.WriteHeader(http.StatusInternalServerError)
			}
			defer reader.Close()

			_, err = io.Copy(res, reader)
			if err != nil {
				// only effective if nothing has been written to res
				res.WriteHeader(http.StatusInternalServerError)
			}
		} else if req.Method == "PUT" {
			err = box.Put(file, req.Body)
			if err != nil {
				res.WriteHeader(http.StatusInternalServerError)
			} else {
				res.WriteHeader(http.StatusOK)
			}
		} else if req.Method == "DELETE" {
			err = box.Delete(file)
			if err != nil {
				res.WriteHeader(http.StatusInternalServerError)
			} else {
				res.WriteHeader(http.StatusOK)
			}
		} else if req.Method == "POST" {
			action := req.FormValue("action")
			loc := req.FormValue("location")

			locDir, err := filer.GetLocation(loc)
			if err != nil {
				res.WriteHeader(http.StatusBadRequest)
				return
			}

			if action == "extract" {
				err = box.Extract(file, locDir)
			} else if action == "archive" {
				err = box.DoArchive(file, locDir)
			} else {
				res.WriteHeader(http.StatusBadRequest)
				return
			}

			if err != nil {
				res.WriteHeader(http.StatusInternalServerError)
			} else {
				res.WriteHeader(http.StatusOK)
			}
		}
	}))

	err = http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", port), nil)
	if err != nil {
		fmt.Printf("ListenAndServe Error :" + err.Error())
	}
}

func readRunFlags(args []string) (port int, pidFile string) {
	flags := flag.NewFlagSet("run", flag.ContinueOnError)
	flags.IntVar(&port, "http-port", 8080, "port to start http interface")
	flags.StringVar(&pidFile, "pid-file", "filer.pid", "pid file of this process")

	err := flags.Parse(args)
	if err != nil {
		fmt.Println(err.Error())
		usage(os.Args[0], 1)
	}
	return
}

func configurationReloader(filer domain.Filer) {
	ticker := time.NewTicker(1 * time.Second)
	for {
		_, ok := <-ticker.C
		if !ok {
			return
		} else {
			filer.Reload()
		}
	}
}

func createPidFile(pidFile string) error {
	if _, err := ioutil.ReadFile(pidFile); err == nil {
		return fmt.Errorf("pid file found, ensure filer is not running and then delete %s", pidFile)
	}

	file, err := os.Create(pidFile)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = fmt.Fprintf(file, "%d", os.Getpid())
	return err
}

func removePidFile(pidFile string) {
	os.Remove(pidFile)
}
