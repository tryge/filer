package main

import (
	"bitbucket.org/tryge/filer/domain"
	"fmt"
	"os"
)

func helpAddLocation() {
	fmt.Printf("Synopsis: %s add-location <name> <directory>", os.Args[0]+" [--config=<db>]")
	fmt.Println()
	fmt.Println()
	fmt.Println("\tAdds a new location to this filer.")
	fmt.Println()
}

func addLocation(filer domain.Filer, args []string) {
	if len(args) != 2 {
		help([]string{"add-location"}, 1)
	}

	err := filer.AddLocation(args[0], args[1])
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(3)
	}
}

func helpChangeLocation() {
	fmt.Printf("Synopsis: %s change-location <name> <directory>", os.Args[0]+" [--config=<db>]")
	fmt.Println()
	fmt.Println()
	fmt.Println("\tChanges the directory of an existing location.")
	fmt.Println()
}

func changeLocation(filer domain.Filer, args []string) {
	if len(args) != 2 {
		help([]string{"change-location"}, 1)
	}

	err := filer.ChangeLocation(args[0], args[1])
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(3)
	}
}

func helpDeleteLocation() {
	fmt.Printf("Synopsis: %s remove-location <name>", os.Args[0]+" [--config=<db>]")
	fmt.Println()
	fmt.Println()
	fmt.Println("\tDeletes the specified location.")
	fmt.Println()
}

func deleteLocation(filer domain.Filer, args []string) {
	if len(args) != 1 {
		help([]string{"remove-location"}, 1)
	}

	err := filer.DeleteLocation(args[0])
	if err != nil {
		fmt.Println("unexpected error: " + err.Error())
		os.Exit(3)
	}
}
