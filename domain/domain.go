package domain

import (
	"io"
	"os"
	"time"
)

type Archive struct {
	path         string
	LastModified time.Time
	Size         int64
}

func (a *Archive) Reader() (io.ReadCloser, error) {
	return os.Open(a.path)
}

type BoxConfig struct {
	Name    string
	RootDir string

	MaxSize         int64
	MaxArchiveSize  int64
	MaxArchiveCount int
}

type Box interface {
	List() ([]Archive, error)
	Delete(name string) error
	DoArchive(name string, srcPath string) error
	Extract(name string, dstPath string) error
	Get(name string) (Archive, error)
	Put(name string, reader io.Reader) error
	Size() int64
	Sync() error
}

type Filer interface {
	AddBox(boxConfig *BoxConfig) error
	ReadBox(boxConfig *BoxConfig) error
	UpdateBox(boxConfig *BoxConfig) error
	DeleteBox(name string) error
	GetBox(name string) (Box, error)
	AddLocation(name, dir string) error
	ChangeLocation(name, dir string) error
	DeleteLocation(name string) error
	GetLocation(name string) (string, error)
	List() ([]Box, error)
	Reload() error
}
