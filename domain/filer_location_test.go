package domain

import (
	. "launchpad.net/gocheck"
	"path/filepath"
)

func (s *FilerSuite) locDir() string {
	return filepath.Join(s.dir, "loc")
}

func (s *FilerSuite) TestLocationSchema(c *C) {
	db := s.openDb(c)
	defer db.Close()

	rows, err := db.Query("select * from locations")
	c.Assert(err, IsNil)
	defer rows.Close()

	cols, err := rows.Columns()
	c.Assert(err, IsNil)

	colMap := make(map[string]struct{})
	for _, col := range cols {
		colMap[col] = struct{}{}
	}

	assertColumn(c, colMap, "name")
	assertColumn(c, colMap, "dir")
}

func (s *FilerSuite) TestAddLocation(c *C) {
	location := "test"
	locationDir := ""

	s.filer.AddLocation(location, s.locDir())

	db := s.openDb(c)
	defer db.Close()

	rows, err := db.Query("select dir from locations where name = ?", &location)
	c.Assert(err, IsNil)
	c.Assert(rows.Next(), Equals, true)

	err = rows.Scan(&locationDir)
	c.Assert(err, IsNil)
	c.Assert(locationDir, Equals, s.locDir())
}

func (s *FilerSuite) TestGetLocation(c *C) {
	location := "test"

	s.filer.AddLocation(location, s.locDir())

	dir, err := s.filer.GetLocation(location)
	c.Assert(err, IsNil)
	c.Assert(dir, Equals, s.locDir())
}

func (s *FilerSuite) TestChangeLocation(c *C) {
	location := "test"

	err := s.filer.AddLocation(location, s.boxDir())
	c.Assert(err, IsNil)

	err = s.filer.ChangeLocation(location, s.locDir())
	c.Assert(err, IsNil)

	dir, err := s.filer.GetLocation(location)
	c.Assert(err, IsNil)
	c.Assert(dir, Equals, s.locDir())
}

func (s *FilerSuite) TestDeleteLocation(c *C) {
	location := "test"

	err := s.filer.AddLocation(location, s.locDir())
	c.Assert(err, IsNil)

	err = s.filer.DeleteLocation(location)
	c.Assert(err, IsNil)

	_, err = s.filer.GetLocation(location)
	c.Assert(err, NotNil)
}

func (s *FilerSuite) TestNoticeAddedLocation(c *C) {
	location := "test"
	locationDir := s.locDir()

	db := s.openDb(c)
	defer db.Close()

	_, err := db.Exec("INSERT INTO locations (name, dir) VALUES(?, ?)", &location, &locationDir)
	c.Assert(err, IsNil)

	s.filer.Reload()

	dir, err := s.filer.GetLocation(location)
	c.Assert(err, IsNil)
	c.Assert(dir, Equals, locationDir)
}

func (s *FilerSuite) TestNoticeDeletedLocation(c *C) {
	location := "test"

	err := s.filer.AddLocation(location, s.locDir())
	c.Assert(err, IsNil)

	db := s.openDb(c)
	defer db.Close()

	_, err = db.Exec("DELETE FROM locations WHERE name = ?", &location)
	c.Assert(err, IsNil)

	s.filer.Reload()

	_, err = s.filer.GetLocation(location)
	c.Assert(err, NotNil)
}
