package domain

import (
	"archive/zip"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

var newBox = func(config *BoxConfig) (*filedBox, error) {
	info, err := os.Stat(config.RootDir)
	if err != nil {
		return nil, err
	}
	if strings.TrimSpace(config.Name) == "" {
		return nil, errors.New("box: config.Name must not be empty")
	}
	if !info.IsDir() {
		return nil, errors.New("box: config.RootDir is not a directory")
	}

	box := &filedBox{
		config.Name,
		config.RootDir,
		config.MaxSize,
		config.MaxArchiveSize,
		config.MaxArchiveCount,
	}
	box.makeConsistentConfig()
	return box, nil
}

type filedBox struct {
	name    string
	rootDir string

	maxSize         int64
	maxArchiveSize  int64
	maxArchiveCount int
}

func (box *filedBox) makeConsistentConfig() {
	if box.maxSize > 0 && (box.maxArchiveSize <= 0 || box.maxArchiveSize > box.maxSize) {
		box.maxArchiveSize = box.maxSize
	}
}

type ByModTime []Archive

func (a ByModTime) Len() int           { return len(a) }
func (a ByModTime) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByModTime) Less(i, j int) bool { return a[i].LastModified.Before(a[j].LastModified) }

func (box *filedBox) Sync() error {
	return box.enforceRestrictions()
}

func (box *filedBox) enforceRestrictions() error {
	if err := box.enforceFileCount(); err != nil {
		return err
	}
	return box.enforceBoxSize()
}

func (box *filedBox) enforceFileCount() error {
	if box.maxArchiveCount <= 0 {
		return nil
	}

	archives, err := box.List()
	if err != nil {
		return err
	}

	sort.Sort(ByModTime(archives))

	for len(archives) > box.maxArchiveCount {
		old := archives[0]
		archives = archives[1:]

		if err = os.Remove(old.path); err != nil {
			return err
		}
	}

	return nil
}

func (box *filedBox) limitFileSize(r io.Reader) io.Reader {
	if box.maxArchiveSize <= 0 {
		return r
	} else {
		return io.LimitReader(r, box.maxArchiveSize)
	}
}

func (box *filedBox) enforceFileSize(r io.Reader, filePath string) error {
	buf := make([]byte, 1)

	if _, err := r.Read(buf); err == nil {
		os.Remove(filePath)

		return fmt.Errorf("box: archive is too big")
	}
	return nil
}

func (box *filedBox) enforceBoxSize() error {
	if box.maxSize <= 0 {
		return nil
	}

	archives, err := box.List()
	if err != nil {
		return err
	}

	sort.Sort(ByModTime(archives))

	size := calcSize(archives)

	for size > box.maxSize {
		old := archives[0]
		archives = archives[1:]

		if err = os.Remove(old.path); err != nil {
			return err
		}

		size -= old.Size
	}

	return nil
}

func calcSize(archives []Archive) int64 {
	var size int64

	for _, archive := range archives {
		size += archive.Size
	}

	return size
}

func (box *filedBox) Delete(name string) error {
	filePath, _, err := box.checkBoxedArchive(name)
	if os.IsNotExist(err) {
		return nil
	} else if err != nil {
		return err
	}

	return os.Remove(filePath)
}

func (box *filedBox) DoArchive(name string, srcPath string) error {
	filePath, fileParent, err := box.checkBoxedArchive(name)
	if !os.IsNotExist(err) {
		return fmt.Errorf("box: archive %s already exists", name)
	}

	info, err := os.Stat(srcPath)
	if err != nil {
		return err
	}
	if !info.IsDir() {
		return fmt.Errorf("box: source %s is not a directory", srcPath)
	}

	if fileParent != box.rootDir {
		os.MkdirAll(fileParent, 0777)
	}

	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	w := zip.NewWriter(file)
	defer w.Close()

	err = filepath.Walk(srcPath, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			relPath, err := filepath.Rel(srcPath, path)
			if err != nil {
				return err
			}

			f, err := w.Create(relPath)
			if err != nil {
				return err
			}

			reader, err := os.Open(path)
			if err != nil {
				return err
			}
			defer reader.Close()

			_, err = io.Copy(f, reader)
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}

	return w.Close()
}

func (box *filedBox) Extract(name string, dstPath string) error {
	filePath, _, err := box.checkBoxedArchive(name)
	if err != nil {
		return err
	}

	info, err := os.Stat(dstPath)
	if os.IsNotExist(err) {
		return fmt.Errorf("box: destination %s does not exist", dstPath)
	} else if err != nil {
		return err
	}
	if !info.IsDir() {
		return fmt.Errorf("box: destination %s is not a directory", dstPath)
	}

	zipFile, err := zip.OpenReader(filePath)
	if err != nil {
		return err
	}
	defer zipFile.Close()

	for _, f := range zipFile.File {
		dstFile := filepath.Join(dstPath, f.Name)
		dstParent := filepath.Dir(filePath)

		if dstParent != dstPath {
			os.MkdirAll(dstParent, 0777)
		}

		dstWriter, err := os.Create(dstFile)
		if err != nil {
			return err
		} else {
			defer dstWriter.Close()

			zipEntry, err := f.Open()
			if err != nil {
				return err
			}
			defer zipEntry.Close()

			io.Copy(dstWriter, zipEntry)
		}
	}
	return nil
}

func (box *filedBox) Get(name string) (Archive, error) {
	filePath, _, err := box.checkBoxedArchive(name)
	if os.IsNotExist(err) {
		return Archive{}, fmt.Errorf("box: archive %s does not exist", name)
	} else if err != nil {
		return Archive{}, fmt.Errorf("box: unexpected error: %s", err.Error())
	}

	info, _ := os.Stat(filePath)
	return Archive{filePath, info.ModTime(), info.Size()}, nil
}

func (box *filedBox) Put(name string, reader io.Reader) error {
	filePath, fileParent, err := box.checkBoxedArchive(name)
	if !os.IsNotExist(err) {
		return fmt.Errorf("box: archive %s already exists", name)
	}

	if fileParent != box.rootDir {
		os.MkdirAll(fileParent, 0777)
	}

	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	if _, err = io.Copy(file, box.limitFileSize(reader)); err != nil {
		return err
	}
	file.Close()

	if err = box.enforceFileSize(reader, filePath); err != nil {
		return err
	}

	return box.enforceRestrictions()
}

func (box *filedBox) checkBoxedArchive(name string) (string, string, error) {
	filePath := filepath.Join(box.rootDir, name)

	fileParent := filepath.Dir(filePath)
	if !strings.HasPrefix(fileParent, box.rootDir) {
		return filePath, fileParent, fmt.Errorf("box: illegal archive name: %s", name)
	}

	info, err := os.Stat(filePath)
	if err != nil {
		// forward error so that caller can decide what to do
		// (could be desired)
		return filePath, fileParent, err
	}
	if info.IsDir() {
		return filePath, fileParent, fmt.Errorf("box: %s is a directory but a file is required", filePath)
	}

	return filePath, fileParent, nil
}

func (box *filedBox) List() ([]Archive, error) {
	archives := make([]Archive, 0)

	err := filepath.Walk(box.rootDir, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			archives = append(archives, Archive{path, info.ModTime(), info.Size()})
		}
		return nil
	})

	return archives, err
}

func (box *filedBox) Size() int64 {
	var size int64

	filepath.Walk(box.rootDir, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			size += info.Size()
		}
		return nil
	})

	return size
}
