package domain

import (
	"database/sql"
	"io/ioutil"
	. "launchpad.net/gocheck"
	"os"
	"path/filepath"
)

var _ = Suite(&FilerSuite{})

type FilerSuite struct {
	dir   string
	filer Filer
}

func (s *FilerSuite) configFile() string {
	return filepath.Join(s.dir, "filer.db")
}

func (s *FilerSuite) boxDir() string {
	return filepath.Join(s.dir, "box")
}

func (s *FilerSuite) SetUpTest(c *C) {
	var err error

	s.dir, err = ioutil.TempDir("", "test")
	if err != nil {
		c.Fatal(err)
	}
	err = os.Mkdir(s.boxDir(), 0777)
	if err != nil {
		c.Fatal(err)
	}
	err = os.Mkdir(s.locDir(), 0777)
	if err != nil {
		c.Fatal(err)
	}

	f, err := NewFiler(s.configFile())
	c.Assert(err, IsNil)
	c.Assert(f, NotNil)

	s.filer = f
}

func (s *FilerSuite) TearDownTest(c *C) {
	os.RemoveAll(s.dir)
}

func (s *FilerSuite) TestDatabaseSchemaInitialized(c *C) {
	db := s.openDb(c)
	defer db.Close()

	rows, err := db.Query("select * from boxes")
	c.Assert(err, IsNil)
	defer rows.Close()

	cols, err := rows.Columns()
	c.Assert(err, IsNil)

	colMap := make(map[string]struct{})
	for _, col := range cols {
		colMap[col] = struct{}{}
	}

	assertColumn(c, colMap, "name")
	assertColumn(c, colMap, "root_dir")
	assertColumn(c, colMap, "max_size")
	assertColumn(c, colMap, "max_archive_size")
	assertColumn(c, colMap, "max_archive_count")
}

func assertColumn(c *C, cols map[string]struct{}, name string) {
	_, ok := cols[name]
	c.Assert(ok, Equals, true)
}

func (s *FilerSuite) TestNoBoxesAfterInitialization(c *C) {
	boxes, err := s.filer.List()
	c.Assert(err, IsNil)
	c.Assert(len(boxes), Equals, 0)
}

func (s *FilerSuite) TestNoticeAddedBox(c *C) {
	s.addBox(c, "testing")

	err := s.filer.Reload()
	c.Assert(err, IsNil)

	boxes, err := s.filer.List()
	c.Assert(err, IsNil)
	c.Assert(len(boxes), Equals, 1)
}

func (s *FilerSuite) TestNoticeUpdateBox(c *C) {
	s.addBox(c, "testing")

	err := s.filer.Reload()
	c.Assert(err, IsNil)

	s.setRestrictions(c, "testing", 3, 2, 4)

	err = s.filer.Reload()
	c.Assert(err, IsNil)

	boxes, err := s.filer.List()
	c.Assert(err, IsNil)
	c.Assert(len(boxes), Equals, 1)

	box := boxes[0].(*filedBox)
	c.Assert(box.maxSize, Equals, int64(3))
	c.Assert(box.maxArchiveSize, Equals, int64(2))
	c.Assert(box.maxArchiveCount, Equals, 4)
}

func (s *FilerSuite) TestNoticeDeletedBox(c *C) {
	s.addBox(c, "testing")

	err := s.filer.Reload()
	c.Assert(err, IsNil)

	s.deleteBox(c, "testing")

	err = s.filer.Reload()
	c.Assert(err, IsNil)

	boxes, err := s.filer.List()
	c.Assert(err, IsNil)
	c.Assert(len(boxes), Equals, 0)
}

func (s *FilerSuite) TestAddBoxApi(c *C) {
	boxConfig := &BoxConfig{"testing", s.boxDir(), 5, 4, 3}

	s.filer.AddBox(boxConfig)

	db := s.openDb(c)
	defer db.Close()

	rows, err := db.Query("SELECT name, root_dir, max_size, max_archive_size, max_archive_count FROM boxes")
	c.Assert(err, IsNil)
	c.Assert(rows.Next(), Equals, true)

	readConfig := BoxConfig{}
	err = rows.Scan(&readConfig.Name, &readConfig.RootDir, &readConfig.MaxSize, &readConfig.MaxArchiveSize, &readConfig.MaxArchiveCount)
	c.Assert(err, IsNil)
	c.Assert(readConfig, DeepEquals, *boxConfig)
}

func (s *FilerSuite) TestDeleteBoxApi(c *C) {
	s.addBox(c, "testing")

	err := s.filer.Reload()
	c.Assert(err, IsNil)

	err = s.filer.DeleteBox("testing")
	c.Assert(err, IsNil)

	boxes, err := s.filer.List()
	c.Assert(err, IsNil)
	c.Assert(len(boxes), Equals, 0)

	db := s.openDb(c)
	defer db.Close()

	rows, err := db.Query("SELECT name, root_dir, max_size, max_archive_size, max_archive_count FROM boxes")
	c.Assert(err, IsNil)
	c.Assert(rows.Next(), Equals, false)
}

func (s *FilerSuite) TestReadNonExistingBox(c *C) {
	s.addBox(c, "testing")

	err := s.filer.Reload()
	c.Assert(err, IsNil)

	boxConfig := &BoxConfig{Name: "testing2"}

	err = s.filer.ReadBox(boxConfig)
	c.Assert(err, NotNil)
}

func (s *FilerSuite) TestReadExistingBox(c *C) {
	boxConfig := &BoxConfig{"testing", s.boxDir(), 5, 4, 3}

	s.filer.AddBox(boxConfig)

	boxConfigR := &BoxConfig{Name: "testing"}

	err := s.filer.ReadBox(boxConfigR)
	c.Assert(err, IsNil)
	c.Assert(boxConfig, DeepEquals, boxConfigR)
}

func (s *FilerSuite) TestUpdateExistingBox(c *C) {
	boxConfig := &BoxConfig{"testing", s.boxDir(), 5, 4, 3}

	err := s.filer.AddBox(boxConfig)
	c.Assert(err, IsNil)

	boxConfig.MaxSize = 10
	err = s.filer.UpdateBox(boxConfig)
	c.Assert(err, IsNil)

	boxConfigR := &BoxConfig{Name: "testing"}
	err = s.filer.ReadBox(boxConfigR)
	c.Assert(err, IsNil)

	c.Assert(boxConfigR, DeepEquals, boxConfig)
}

func (s *FilerSuite) TestUpdateNonExistingBox(c *C) {
	boxConfig := &BoxConfig{"testing", s.boxDir(), 5, 4, 3}

	err := s.filer.UpdateBox(boxConfig)
	c.Assert(err, NotNil)
}

func (s *FilerSuite) addBox(c *C, name string) {
	db := s.openDb(c)
	defer db.Close()

	_, err := db.Exec("INSERT INTO boxes (name, root_dir) VALUES ('" + name + "', '" + s.boxDir() + "')")
	c.Assert(err, IsNil)
}

func (s *FilerSuite) deleteBox(c *C, name string) {
	db := s.openDb(c)
	defer db.Close()

	_, err := db.Exec("DELETE FROM boxes WHERE name = ?", &name)
	c.Assert(err, IsNil)
}

func (s *FilerSuite) setRestrictions(c *C, name string, maxSize, maxArchiveSize int64, maxArchiveCount int) {
	db := s.openDb(c)
	defer db.Close()

	_, err := db.Exec("UPDATE boxes SET max_size = ?, max_archive_size = ?, max_archive_count = ? WHERE name = ?", &maxSize, &maxArchiveSize, &maxArchiveCount, &name)
	c.Assert(err, IsNil)
}

func (s *FilerSuite) openDb(c *C) *sql.DB {
	db, err := sql.Open("sqlite3", s.configFile())
	c.Assert(err, IsNil)
	c.Assert(db, NotNil)
	return db
}
