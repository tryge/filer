package domain

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"sync"
)

const (
	createBoxesTable = `CREATE TABLE IF NOT EXISTS boxes (
		name    VARCHAR(255) PRIMARY KEY NOT NULL,
		root_dir TEXT NOT NULL,
		max_size INTEGER DEFAULT 0,
		max_archive_size INTEGER DEFAULT 0,
		max_archive_count INTEGER DEFAULT 0
	)`
	createLocationsTable = `CREATE TABLE IF NOT EXISTS locations (
		name VARCHAR(255) PRIMARY KEY NOT NULL,
		dir TEXT NOT NULL
	)`

	selectBoxes = `SELECT name, root_dir, max_size, max_archive_size, max_archive_count FROM boxes`
	insertBox   = `INSERT INTO boxes (name, root_dir, max_size, max_archive_size, max_archive_count) VALUES(?, ?, ?, ?, ?)`
	updateBox   = `UPDATE boxes SET root_dir=?, max_size=?, max_archive_size=?, max_archive_count=? WHERE name=?`
	deleteBox   = `DELETE FROM boxes WHERE name = ?`

	selectLocations = `SELECT name, dir FROM locations`
	insertLocation  = `INSERT INTO locations (name, dir) VALUES (?, ?)`
	updateLocation  = `UPDATE locations SET dir = ? WHERE name = ?`
	deleteLocation  = `DELETE FROM locations WHERE name = ?`
)

type dber func(db *sql.DB) error

type gateway struct {
	config string
	once   sync.Once
}

func (g *gateway) withDb(dber dber) error {
	db, err := sql.Open("sqlite3", g.config)
	if err != nil {
		return fmt.Errorf("filer: could not open configuration database %s: %s", g.config, err.Error())
	}
	defer db.Close()

	return dber(db)
}

func (g *gateway) initDb(db *sql.DB) error {
	_, err := db.Exec(createBoxesTable)
	if err != nil {
		return fmt.Errorf("filer: could not initialize configuration database: %s", err.Error())
	}
	_, err = db.Exec(createLocationsTable)
	if err != nil {
		return fmt.Errorf("filer: could not initialize configuration database: %s", err.Error())
	}
	return nil
}

func (g *gateway) reload() (locations map[string]string, boxes []*filedBox, err error) {
	g.withDb(func(db *sql.DB) error {
		g.once.Do(func() {
			err = g.initDb(db)
		})
		if err != nil {
			return err
		}

		locations, err = g.reloadLocations(db)
		if err != nil {
			return err
		}

		boxes, err = g.reloadBoxes(db)
		if err != nil {
			return err
		}
		return nil
	})
	return
}

func (g *gateway) reloadLocations(db *sql.DB) (map[string]string, error) {
	locationRows, err := db.Query(selectLocations)
	if err != nil {
		return nil, fmt.Errorf("filer: could not read locations configuration from database: %s", err.Error())
	}
	defer locationRows.Close()

	locations := make(map[string]string)
	for locationRows.Next() {
		var name, dir string

		err = locationRows.Scan(&name, &dir)
		if err != nil {
			return nil, fmt.Errorf("filer: could not read locations query result: %s", err.Error())
		}

		locations[name] = dir
	}
	return locations, nil
}

func (g *gateway) reloadBoxes(db *sql.DB) ([]*filedBox, error) {
	rows, err := db.Query(selectBoxes)
	if err != nil {
		return nil, fmt.Errorf("filer: could not read boxes configuration from database: %s", err.Error())
	}
	defer rows.Close()

	boxes := make([]*filedBox, 0)

	for rows.Next() {
		var bc BoxConfig

		err = rows.Scan(&bc.Name, &bc.RootDir, &bc.MaxSize, &bc.MaxArchiveSize, &bc.MaxArchiveCount)
		if err != nil {
			return nil, fmt.Errorf("filer: could not scan row: %s", err.Error())
		}

		box, err := newBox(&bc)
		if err != nil {
			return nil, err
		}

		boxes = append(boxes, box)
	}

	return boxes, nil
}

func (g *gateway) addBox(boxConfig *BoxConfig) error {
	return g.withDb(func(db *sql.DB) error {
		_, err := db.Exec(insertBox,
			&boxConfig.Name,
			&boxConfig.RootDir,
			&boxConfig.MaxSize,
			&boxConfig.MaxArchiveSize,
			&boxConfig.MaxArchiveCount,
		)

		if err != nil {
			return fmt.Errorf("filer could not insert new box %s: %s", boxConfig.Name, err.Error())
		}
		return nil
	})
}

func (g *gateway) changeBox(boxConfig *BoxConfig) error {
	return g.withDb(func(db *sql.DB) error {
		_, err := db.Exec(updateBox,
			&boxConfig.RootDir,
			&boxConfig.MaxSize,
			&boxConfig.MaxArchiveSize,
			&boxConfig.MaxArchiveCount,
			&boxConfig.Name,
		)
		if err != nil {
			return fmt.Errorf("filer: could not update box: %s", err.Error())
		}
		return nil
	})
}

func (g *gateway) deleteBox(name string) error {
	return g.withDb(func(db *sql.DB) error {
		_, err := db.Exec(deleteBox, name)
		if err != nil {
			return fmt.Errorf("filer: could not delete box: %s", err.Error())
		}
		return nil
	})
}

func (g *gateway) addLocation(name, location string) error {
	return g.withDb(func(db *sql.DB) error {
		_, err := db.Exec(insertLocation, &name, &location)
		return err
	})
}

func (g *gateway) changeLocation(name, location string) error {
	return g.withDb(func(db *sql.DB) error {
		res, err := db.Exec(updateLocation, &location, &name)
		if err != nil {
			return err
		}

		num, err := res.RowsAffected()
		if err != nil {
			return err
		}
		if num != 1 {
			return fmt.Errorf("filer: location %s does not exist", name)
		}
		return nil
	})
}

func (g *gateway) deleteLocation(name string) error {
	return g.withDb(func(db *sql.DB) error {
		_, err := db.Exec(deleteLocation, &name)
		return err
	})
}
