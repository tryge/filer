package domain

import (
	"fmt"
	"os"
	"sync"
)

var NewFiler = func(config string) (Filer, error) {
	filer := &filer{
		gateway:   &gateway{config: config},
		boxes:     make([]*filedBox, 0),
		locations: make(map[string]string),
	}
	return filer, filer.Reload()
}

type filer struct {
	gateway   *gateway
	boxes     []*filedBox
	locations map[string]string
	lock      sync.RWMutex
}

func (f *filer) AddBox(boxConfig *BoxConfig) error {
	box, err := newBox(boxConfig)
	if err != nil {
		return err
	}

	err = f.gateway.addBox(boxConfig)
	if err == nil {
		f.lock.Lock()
		defer f.lock.Unlock()

		f.boxes = append(f.boxes, box)
	}
	return err
}

func (f *filer) GetBox(name string) (box Box, err error) {
	err = f.withBox(name, func(pos int, b *filedBox) (bool, error) {
		box = b

		if box == nil {
			return true, fmt.Errorf("filer: box %s does not exist", name)
		} else {
			return true, nil
		}
	})
	return box, err
}

func (f *filer) ReadBox(boxConfig *BoxConfig) error {
	return f.withBox(boxConfig.Name, func(pos int, box *filedBox) (bool, error) {
		if box == nil {
			return true, fmt.Errorf("filer: box %s does not exist", boxConfig.Name)
		} else {
			boxConfig.RootDir = box.rootDir
			boxConfig.MaxSize = box.maxSize
			boxConfig.MaxArchiveSize = box.maxArchiveSize
			boxConfig.MaxArchiveCount = box.maxArchiveCount
			return true, nil
		}
	})
}

func (f *filer) UpdateBox(boxConfig *BoxConfig) error {
	return f.withBox(boxConfig.Name, func(pos int, box *filedBox) (bool, error) {
		if box == nil {
			return true, fmt.Errorf("filer: box %s doesn't exist", boxConfig.Name)
		}

		err := f.gateway.changeBox(boxConfig)
		if err == nil {
			f.lock.RUnlock()
			f.lock.Lock()
			defer f.lock.Unlock()

			tmp, err := newBox(boxConfig)
			if err != nil {
				return false, err
			}
			f.boxes[pos] = tmp

			return false, nil
		}
		return true, err
	})
}

func (f *filer) DeleteBox(name string) error {
	return f.withBox(name, func(pos int, box *filedBox) (unlock bool, err error) {
		unlock = true

		if box != nil {
			err = f.gateway.deleteBox(name)
			if err == nil {
				f.lock.RUnlock()
				unlock = false

				f.lock.Lock()
				defer f.lock.Unlock()

				f.boxes = append(f.boxes[:pos], f.boxes[pos+1:]...)
			}
		}
		return
	})
}

func (f *filer) AddLocation(name, location string) error {
	info, err := os.Stat(location)
	if err != nil {
		return err
	}
	if !info.IsDir() {
		return fmt.Errorf("filer: location %s is not a directory", location)
	}

	err = f.gateway.addLocation(name, location)
	if err == nil {
		f.lock.Lock()
		defer f.lock.Unlock()

		f.locations[name] = location
	}
	return err
}

func (f *filer) ChangeLocation(name, location string) error {
	err := f.gateway.changeLocation(name, location)
	if err == nil {
		f.lock.Lock()
		defer f.lock.Unlock()

		f.locations[name] = location
	}
	return err
}

func (f *filer) DeleteLocation(name string) error {
	err := f.gateway.deleteLocation(name)
	if err == nil {
		f.lock.Lock()
		defer f.lock.Unlock()

		delete(f.locations, name)
	}
	return err
}

func (f *filer) GetLocation(name string) (string, error) {
	f.lock.RLock()
	defer f.lock.RUnlock()

	dir, ok := f.locations[name]
	if ok {
		return dir, nil
	} else {
		return "", fmt.Errorf("filer: location %s does not exist", name)
	}
}

func (f *filer) List() ([]Box, error) {
	f.lock.RLock()
	defer f.lock.RUnlock()

	boxes := make([]Box, len(f.boxes))

	for i, box := range f.boxes {
		boxes[i] = box
	}
	return boxes, nil
}

func (f *filer) Reload() error {
	locations, boxes, err := f.gateway.reload()
	if err == nil {
		f.lock.Lock()
		defer f.lock.Unlock()

		f.locations = locations
		f.boxes = boxes

		for _, box := range f.boxes {
			box.Sync()
		}
	}

	return err
}

type Boxer func(pos int, box *filedBox) (bool, error)

func (f *filer) withBox(name string, boxer Boxer) (err error) {
	var unlock bool

	f.lock.RLock()
	defer func() {
		if unlock {
			f.lock.RUnlock()
		}
	}()

	for pos, box := range f.boxes {
		if box.name == name {
			unlock, err = boxer(pos, box)
			return
		}
	}
	unlock, err = boxer(-1, nil)
	return
}
