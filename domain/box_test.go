package domain

import (
	"io/ioutil"
	. "launchpad.net/gocheck"
	"os"
	"path/filepath"
	"strings"
)

// test new boxes

var _ = Suite(&BoxesSuite{})
var _ = Suite(&NewBoxesSuite{})

type NewBoxesSuite struct {
	dir string
}

func (s *NewBoxesSuite) SetUpTest(c *C) {
	var err error

	s.dir, err = ioutil.TempDir("", "test")
	if err != nil {
		c.Fatal(err)
	}
}

func (s *NewBoxesSuite) TearDownTest(c *C) {
	os.RemoveAll(s.dir)
}

// actual test for new boxes

func (s *NewBoxesSuite) TestEmptyNameReturnsError(c *C) {
	_, err := newBox(&BoxConfig{Name: "", RootDir: s.dir})
	c.Assert(err, NotNil)
}

func (s *NewBoxesSuite) TestBlankNameReturnsError(c *C) {
	_, err := newBox(&BoxConfig{Name: " \n\t\r\f", RootDir: s.dir})
	c.Assert(err, NotNil)
}

func (s *NewBoxesSuite) TestRootDirDoesNotExist(c *C) {
	_, err := newBox(&BoxConfig{Name: "testing", RootDir: filepath.Join(s.dir, "does-not-exist")})
	c.Assert(err, NotNil)
}

func (s *NewBoxesSuite) TestRootDirNotADirectory(c *C) {
	file, err := os.Create(filepath.Join(s.dir, "file"))
	c.Assert(err, IsNil)
	defer file.Close()

	_, err = newBox(&BoxConfig{Name: "testing", RootDir: filepath.Join(s.dir, "file")})
	c.Assert(err, NotNil)
}

// test boxes

type BoxesSuite struct {
	dir string
	box *filedBox
}

func (s *BoxesSuite) SetUpTest(c *C) {
	var err error

	s.dir, err = ioutil.TempDir("", "test")
	if err != nil {
		c.Fatal(err)
	}

	s.box, err = newBox(&BoxConfig{Name: "testing", RootDir: s.dir})
	if err != nil {
		c.Fatal(err)
	}
}

func (s *BoxesSuite) TearDownTest(c *C) {
	os.RemoveAll(s.dir)
}

// actual test for boxes

func (s *BoxesSuite) TestIsEmpty(c *C) {
	files, err := s.box.List()

	c.Assert(err, IsNil)

	if len(files) != 0 {
		c.Fatal("new box should be empty but List() returns nonempty slice")
	}
}

func (s *BoxesSuite) TestSizeIsZero(c *C) {
	c.Check(s.box.Size(), Equals, int64(0))
}

func (s *BoxesSuite) TestPutDoesNotBreakOut_1(c *C) {
	r := strings.NewReader("test")
	err := s.box.Put("../unique-name", r)

	c.Assert(err, NotNil)
}

func (s *BoxesSuite) TestPutDoesNotBreakOut_2(c *C) {
	r := strings.NewReader("test")
	err := s.box.Put("./../unique-name", r)

	c.Assert(err, NotNil)
}

func (s *BoxesSuite) TestPutDoesNotBreakOut_3(c *C) {
	r := strings.NewReader("test")
	err := s.box.Put("bla/../../unique-name", r)

	c.Assert(err, NotNil)
}

func (s *BoxesSuite) TestPutArchive(c *C) {
	putContent(c, s, "unique-name", "test")

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 1)
	c.Assert(archives[0].Size, Equals, int64(4))
	c.Assert(archives[0].path, Equals, filepath.Join(s.dir, "unique-name"))
}

func (s *BoxesSuite) TestPutAndDeleteArchive(c *C) {
	putContent(c, s, "unique-name", "test")
	deleteArchive(c, s, "unique-name")

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 0)
	c.Assert(s.box.Size(), Equals, int64(0))
}

func (s *BoxesSuite) TestDeleteDoesNotBreakOut(c *C) {
	err := s.box.Delete("../must-not-work")
	c.Assert(err, NotNil)
}

func (s *BoxesSuite) TestPutNestedArchive(c *C) {
	putContent(c, s, "nested/unique-name", "test")

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 1)
	c.Assert(archives[0].Size, Equals, int64(4))
	c.Assert(archives[0].path, Equals, filepath.Join(s.dir, "nested/unique-name"))
}

func (s *BoxesSuite) TestCannotDeleteDirectory(c *C) {
	putContent(c, s, "nested/unique-name", "test")

	err := s.box.Delete("nested")
	c.Assert(err, NotNil)
}

func (s *BoxesSuite) TestSizeAfterPut(c *C) {
	putContent(c, s, "unique-name", "test")

	_, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(s.box.Size(), Equals, int64(4))
}

func (s *BoxesSuite) TestSizeAfterDoublePut(c *C) {
	putContent(c, s, "unique-name", "test")
	putContent(c, s, "unique-name2", "test2")

	_, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(s.box.Size(), Equals, int64(9))
}

func (s *BoxesSuite) TestGetAfterPut(c *C) {
	putContent(c, s, "unique-name", "test")

	content := getContent(c, s, "unique-name")

	c.Assert(content, Equals, "test")
}

func (s *BoxesSuite) TestMaxFileCount(c *C) {
	s.box.maxArchiveCount = 1
	s.box.makeConsistentConfig()

	putContent(c, s, "unique-name", "test")
	putContent(c, s, "unique-name2", "test2")

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 1)
	c.Assert(readContent(c, &archives[0]), Equals, "test2")
}

func (s *BoxesSuite) TestMaxArchiveSize(c *C) {
	s.box.maxArchiveSize = 4
	s.box.makeConsistentConfig()

	r := strings.NewReader("too long")
	err := s.box.Put("unique-name", r)
	c.Assert(err, NotNil)
}

func (s *BoxesSuite) TestMaxBoxSizeForSingleArchive(c *C) {
	s.box.maxSize = 4
	s.box.makeConsistentConfig()

	r := strings.NewReader("too long")
	err := s.box.Put("unique-name", r)
	c.Assert(err, NotNil)
}

func (s *BoxesSuite) TestMaxBoxSizeStrongerThanMaxArchiveSize(c *C) {
	s.box.maxSize = 4
	s.box.maxArchiveSize = 10
	s.box.makeConsistentConfig()

	r := strings.NewReader("too long")
	err := s.box.Put("unique-name", r)
	c.Assert(err, NotNil)
}

func (s *BoxesSuite) TestMaxBoxSizeForTwoArchives(c *C) {
	s.box.maxSize = 5
	s.box.makeConsistentConfig()

	putContent(c, s, "unique-name", "test")
	putContent(c, s, "unique-name2", "test2")

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 1)
	c.Assert(readContent(c, &archives[0]), Equals, "test2")
}

func (s *BoxesSuite) TestSync_1(c *C) {
	putContent(c, s, "unique-name", "test")
	putContent(c, s, "unique-name2", "test2")

	s.box.maxArchiveCount = 1
	s.box.makeConsistentConfig()
	s.box.Sync()

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 1)
	c.Assert(readContent(c, &archives[0]), Equals, "test2")
}

func (s *BoxesSuite) TestSync_2(c *C) {
	putContent(c, s, "unique-name", "test")
	putContent(c, s, "unique-name2", "test2")

	s.box.maxSize = 5
	s.box.makeConsistentConfig()
	s.box.Sync()

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 1)
	c.Assert(readContent(c, &archives[0]), Equals, "test2")
}

func (s *BoxesSuite) TestExtractArchive(c *C) {
	zipFile, err := os.Open("test.zip")
	c.Assert(err, IsNil)
	defer zipFile.Close()

	err = s.box.Put("test.zip", zipFile)
	c.Assert(err, IsNil)

	dir, err := ioutil.TempDir("", "ectractor")
	c.Assert(err, IsNil)
	defer os.RemoveAll(dir)

	err = s.box.Extract("test.zip", dir)
	c.Assert(err, IsNil)

	test1, err := os.Open(filepath.Join(dir, "test.txt"))
	c.Assert(err, IsNil)
	defer test1.Close()

	content1, err := ioutil.ReadAll(test1)
	c.Assert(err, IsNil)
	c.Assert(string(content1), Equals, "this is a test\n\n")

	test2, err := os.Open(filepath.Join(dir, "test2.txt"))
	c.Assert(err, IsNil)
	defer test2.Close()

	content2, err := ioutil.ReadAll(test2)
	c.Assert(err, IsNil)
	c.Assert(string(content2), Equals, "this is another test file\n")
}

func (s *BoxesSuite) TestArchiveEmptyDir(c *C) {
	archiveRoot := filepath.Join(s.dir, "toArchive")
	err := os.Mkdir(archiveRoot, 0777)
	c.Assert(err, IsNil)

	err = s.box.DoArchive("test.zip", archiveRoot)
	c.Assert(err, IsNil)

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 1)
	c.Assert(archives[0].path, Equals, filepath.Join(s.dir, "test.zip"))
}

func (s *BoxesSuite) TestArchiveOneFile(c *C) {
	archiveRoot := filepath.Join(s.dir, "toArchive")
	err := os.Mkdir(archiveRoot, 0777)
	c.Assert(err, IsNil)

	putContent(c, s, "toArchive/single.txt", "single file content")

	err = s.box.DoArchive("test.zip", archiveRoot)
	c.Assert(err, IsNil)

	deleteArchive(c, s, "toArchive/single.txt")

	archives, err := s.box.List()
	c.Assert(err, IsNil)
	c.Assert(len(archives), Equals, 1)
	c.Assert(archives[0].path, Equals, filepath.Join(s.dir, "test.zip"))

	s.box.Extract("test.zip", archiveRoot)

	c.Assert(getContent(c, s, "toArchive/single.txt"), Equals, "single file content")
}

func putContent(c *C, s *BoxesSuite, name, content string) {
	r := strings.NewReader(content)
	err := s.box.Put(name, r)
	c.Assert(err, IsNil)
}

func deleteArchive(c *C, s *BoxesSuite, name string) {
	err := s.box.Delete(name)
	c.Assert(err, IsNil)
}

func getContent(c *C, s *BoxesSuite, name string) string {
	archive, err := s.box.Get(name)
	c.Assert(err, IsNil)

	return readContent(c, &archive)
}

func readContent(c *C, archive *Archive) string {
	reader, err := archive.Reader()
	c.Assert(err, IsNil)
	defer reader.Close()

	content, err := ioutil.ReadAll(reader)
	c.Assert(err, IsNil)
	return string(content)
}
