package main

import (
	"bitbucket.org/tryge/filer/domain"
	"flag"
	"fmt"
	"os"
)

func helpAddBox() {
	fmt.Printf("Synopsis: %s add-box <name> <directory> <options>", os.Args[0]+" [--config=<db>]")
	fmt.Println()
	fmt.Println()
	fmt.Println("\tAdds a new box to this filer.")
	fmt.Println()
	fmt.Println("Options:")
	fmt.Println("\t--max-size <int>       ... the maximum size of the box, default infinite")
	fmt.Println("\t--max-file-size <int>  ... the maximum size of a file within the box, default infinite")
	fmt.Println("\t--max-file-count <int> ... the maximum amount of files within the box, default infinite")
	fmt.Println()
	fmt.Println("Notes:")
	fmt.Println("\tIf the maximum size of the box is set and no maximum size for files is set (or is set to a bigger value)")
	fmt.Println("\tit will be adjusted to the maximum size of the box.")
	fmt.Println()
	fmt.Println("\tIf new files are added to the box that violates the maximum size or maximum file count of the box, files ")
	fmt.Println("\tare deleted until the restrictions are met; the oldest files are deleted first.")
	fmt.Println()
}

func addBox(filer domain.Filer, args []string) {
	if len(args) < 2 {
		help([]string{"add-box"}, 1)
	}

	boxConfig := &domain.BoxConfig{Name: args[0], RootDir: args[1]}

	info, err := os.Stat(boxConfig.RootDir)
	if os.IsNotExist(err) {
		fmt.Println("rootDir " + boxConfig.RootDir + " does not exist.")
		os.Exit(3)
	}
	if err != nil {
		fmt.Println("unexpected error: " + err.Error())
		os.Exit(3)
	}
	if !info.IsDir() {
		fmt.Println("rootDir " + boxConfig.RootDir + " is not a directory")
		os.Exit(3)
	}

	readBoxFlags(boxConfig, args)

	err = filer.AddBox(boxConfig)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(3)
	}
}

func helpChangeBox() {
	fmt.Printf("Synopsis: %s change-box <name> <options>", os.Args[0]+" [--config=<db>]")
	fmt.Println()
	fmt.Println()
	fmt.Println("\tChanges the configuration of an *existing* box, any ommited options remain unchanged.")
	fmt.Println()
	fmt.Println("Options:")
	fmt.Println("\t--max-size <int>       ... the maximum size of the box")
	fmt.Println("\t--max-file-size <int>  ... the maximum size of a file within the box")
	fmt.Println("\t--max-file-count <int> ... the maximum amount of files within the box")
	fmt.Println()
	fmt.Println("Notes:")
	fmt.Println("\tIf the maximum size of the box is set and no maximum size for files is set (or is set to a bigger value)")
	fmt.Println("\tit will be adjusted to the maximum size of the box.")
	fmt.Println()
	fmt.Println("\tIf new files are added to the box that violates the maximum size or maximum file count of the box, files ")
	fmt.Println("\tare deleted until the restrictions are met; the oldest files are deleted first.")
	fmt.Println()
}

func changeBox(filer domain.Filer, args []string) {
	if len(args) < 1 {
		help([]string{"change-box"}, 1)
	}

	boxConfig := &domain.BoxConfig{Name: args[0]}

	err := filer.ReadBox(boxConfig)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(3)
	}

	readBoxFlags(boxConfig, args)

	err = filer.UpdateBox(boxConfig)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(3)
	}
}

func readBoxFlags(boxConfig *domain.BoxConfig, args []string) {
	flags := flag.NewFlagSet("change", flag.ContinueOnError)
	flags.Int64Var(&boxConfig.MaxSize, "max-size", boxConfig.MaxSize, "maximum size of the box")
	flags.Int64Var(&boxConfig.MaxArchiveSize, "max-file-size", boxConfig.MaxArchiveSize, "maximum size of a file within the box")
	flags.IntVar(&boxConfig.MaxArchiveCount, "max-file-count", boxConfig.MaxArchiveCount, "maximum file count of the box")

	err := flags.Parse(args)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(3)
	}
}

func helpDeleteBox() {
	fmt.Printf("Synopsis: %s remove-box <name>", os.Args[0]+" [--config=<db>]")
	fmt.Println()
	fmt.Println()
	fmt.Println("\tDeletes the specified box.")
	fmt.Println()
}

func deleteBox(filer domain.Filer, args []string) {
	if len(args) != 1 {
		help([]string{"remove-box"}, 1)
	}

	err := filer.DeleteBox(args[0])
	if err != nil {
		fmt.Println("unexpected error: " + err.Error())
		os.Exit(3)
	}
}
