package main

import (
	"bitbucket.org/tryge/filer/domain"
	"fmt"
	"os"
	"strings"
)

const VERSION = "1.0"

type commandHandlerFunc func(domain.Filer, []string)

type helpHandlerFunc func()

var commandMap = make(map[string]commandHandlerFunc)

var helpMap = make(map[string]helpHandlerFunc)

func init() {
	commandMap["add-box"] = addBox
	commandMap["change-box"] = changeBox
	commandMap["remove-box"] = deleteBox
	commandMap["add-location"] = addLocation
	commandMap["change-location"] = changeLocation
	commandMap["remove-location"] = deleteLocation
	commandMap["run"] = runFiler

	helpMap["add-box"] = helpAddBox
	helpMap["change-box"] = helpChangeBox
	helpMap["remove-box"] = helpDeleteBox
	helpMap["add-location"] = helpAddLocation
	helpMap["change-location"] = helpChangeLocation
	helpMap["remove-location"] = helpDeleteLocation
	helpMap["run"] = helpRunFiler
	helpMap["help"] = helpHelp
}

func helpHelp() {
	fmt.Printf("Synopsis: %s help [<cmd>...]", os.Args[0])
	fmt.Println()
	fmt.Println()
	fmt.Println("\tShows the help for the specified commands or if none given the general usage.")
	fmt.Println()
}

func help(args []string, exitValue int) {
	if len(args) == 0 {
		usage(os.Args[0], exitValue)
	}

	for pos, arg := range args {
		if pos > 0 {
			fmt.Println()
			fmt.Println()
		}

		handler, ok := helpMap[arg]
		if !ok {
			fmt.Printf("command %s does not exist", arg)
			fmt.Println()
		} else {
			handler()
		}
	}

	os.Exit(exitValue)
}

func usage(cmd string, exitValue int) {
	fmt.Printf("Usage: %s [--config=<config>] <command> [<command-arguments>]", cmd)
	fmt.Println()
	fmt.Println()
	fmt.Println("\tthe flag '--config' is optional, if omitted defaults to 'config.db'")
	fmt.Println()
	fmt.Println("\tcommand can be one of:")
	fmt.Println("\t\trun             ... run filer in foreground")
	fmt.Println("\t\tadd-box         ... add a box")
	fmt.Println("\t\tchange-box      ... change the configuration of a box")
	fmt.Println("\t\tremove-box      ... remove a box")
	fmt.Println("\t\tadd-location    ... add a location")
	fmt.Println("\t\tchange-location ... change a location")
	fmt.Println("\t\tremove-location ... remove a location")
	fmt.Println("\t\thelp            ... print this help")
	fmt.Println()
	fmt.Printf("For more information on a command use %s help <command>", cmd)
	fmt.Println()

	os.Exit(exitValue)
}

func main() {
	if len(os.Args) < 2 {
		usage(os.Args[0], 1)
	}

	var cmdIndex = 1
	var configDb = "config.db"

	if strings.HasPrefix(os.Args[1], "--config=") {
		cmdIndex = 2
		configDb = strings.TrimSpace(os.Args[1][10:])
	}
	if os.Args[1] == "--config" {
		cmdIndex = 3
		configDb = os.Args[2]
	}

	if len(os.Args) <= cmdIndex {
		usage(os.Args[0], 1)
	}

	cmd := os.Args[cmdIndex]
	cmdArgs := os.Args[cmdIndex+1:]

	if cmd == "help" {
		help(cmdArgs, 0)
	}

	filer, err := domain.NewFiler(configDb)
	if err != nil {
		fmt.Printf("error accessing configuration database %s: %s", configDb, err)
		fmt.Println()
		os.Exit(2)
	}

	cmdHandler, ok := commandMap[cmd]
	if !ok {
		usage(os.Args[0], 1)
	} else {
		cmdHandler(filer, cmdArgs)
	}
}
